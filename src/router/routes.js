const routes = [
  { path: '/', component: require('@/components/site/Site') },
  { path: '/entrar', component: require('@/components/login/Login') },
  { path: '/sistema',
    component: require('@/components/Index'),
    children: [
      {
        path: 'consulta',
        component: require('@/components/consulta/Consulta')
      }
    ]
  },
  //  adm
  { path: '/admin', component: require('@/components/admin/Index') }
]

export default routes
