import { sync } from 'vuex-router-sync'

// import components vue
import Vue from 'vue'
import Vuex from 'vuex'
import VueResouce from 'vue-resource'
import VueRouter from 'vue-router'

// import 'vue-material/dist/vue-material.css'
// import template
import App from './App'
// import configs
import FirebaseInit from './firebase/firebase-init'
import routes from './router/routes'
import VuexStore from './vuex/store'
import VueMaterial from 'vue-material'

// use components vue
Vue.use(Vuex)
Vue.use(VueResouce)
Vue.use(VueRouter)
Vue.use(VueMaterial)

// set http options root to env var SERVER
Vue.http.options.root = process.env.SERVER

const store = new Vuex.Store(VuexStore)

// loading routes
const router = new VueRouter({
  routes
})

// validando rotas
router.beforeEach((to, from, next) => {
  // does NOT have access to `this` component instance
  const allow = ['/entrar', '/']
  FirebaseInit.auth().onAuthStateChanged(user => {
    if (user) {
      !allow.includes(to.path) ? next() : next('/sistema')
    } else {
      !allow.includes(to.path) ? next('/entrar') : next()
    }
  })
})

sync(store, router)

// create instance vue
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  styles: [
    require('vue-material/dist/vue-material.css')
  ],
  scripts: [
    require('vue-material/dist/vue-material.js')
  ]
})
