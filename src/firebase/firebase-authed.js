import FirebaseInit from './firebase-init.js'

class FirebaseAuthed {
  constructor () {
    
  }
  usuarioAtual () {
    FirebaseInit.auth().onAuthStateChanged(user => {
      if (user) {
				console.log('logado', user) 
				return user
      } else {
        console.log('não logado')
      }
    })
  }

  sair () {
    FirebaseInit.auth().signOut()
  }
}

export default FirebaseAuthed
